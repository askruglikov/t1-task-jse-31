package ru.t1.kruglikov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.kruglikov.tm.dto.request.ServerAboutRequest;
import ru.t1.kruglikov.tm.dto.request.ServerVersionRequest;
import ru.t1.kruglikov.tm.dto.response.ServerAboutResponse;
import ru.t1.kruglikov.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}

