package ru.t1.kruglikov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import org.jetbrains.annotations.NotNull;
import ru.t1.kruglikov.tm.model.Project;
import ru.t1.kruglikov.tm.model.Task;
import ru.t1.kruglikov.tm.model.User;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@XmlType(name = "domain")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonXmlRootElement(localName = "domain")
@Getter
@Setter
@NoArgsConstructor @XmlRootElement
public class Domain implements Serializable {

    private static final long serialVersionUID = 1;

    @NotNull private String id = UUID.randomUUID().toString();
    @NotNull private Date created = new Date();

    @JsonProperty("user")
    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    @JacksonXmlElementWrapper(localName = "users")
    @NotNull private List<User> users = new ArrayList<>();

    @JsonProperty("project")
    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    @JacksonXmlElementWrapper(localName = "projects")
    @NotNull private List<Project> projects = new ArrayList<>();

    @JsonProperty("task")
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    @JacksonXmlElementWrapper(localName = "tasks")
    @NotNull private List<Task> tasks = new ArrayList<>();

}
