package ru.t1.kruglikov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.enumerated.Role;
import ru.t1.kruglikov.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    @NotNull private final String NAME = "user-remove";
    @NotNull private final String DESCRIPTION = "User remove.";

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");

        System.out.println("Enter login:");
        @Nullable final String login = TerminalUtil.nextLine();

        getUserService().removeOneByLogin(login);
    }

    @NotNull @Override
    public String getName() {
        return NAME;
    }

    @NotNull @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
