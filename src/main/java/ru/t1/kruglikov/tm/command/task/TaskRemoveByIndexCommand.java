package ru.t1.kruglikov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.kruglikov.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull public static final String NAME = "task-remove-by-index";
    @NotNull public static final String DESCRIPTION = "Remove task by index.";

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");

        System.out.println("Enter task index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        getTaskService().removeOneByIndex(getAuthService().getUserId(), index);
    }

    @NotNull @Override
    public String getName() {
        return NAME;
    }

    @NotNull @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
