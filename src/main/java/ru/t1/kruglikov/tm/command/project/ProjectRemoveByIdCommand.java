package ru.t1.kruglikov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.model.Project;
import ru.t1.kruglikov.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull public static final String NAME = "project-remove-by-id";
    @NotNull public static final String DESCRIPTION = "Remove project by id.";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");

        System.out.println("Enter project id:");
        @Nullable final String id = TerminalUtil.nextLine();

        @Nullable final Project project = getProjectService().findOneById(id);
        getProjectTaskService().removeProjectById(getAuthService().getUserId(), project.getId());
    }

    @NotNull @Override
    public String getName() {
        return NAME;
    }

    @NotNull @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
