package ru.t1.kruglikov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.kruglikov.tm.dto.Domain;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull public static final String NAME = "data-load-bin";
    @NotNull public static final String DESCRIPTION = "Load data from binary file.";

    @SneakyThrows @Override
    public void execute() {
        System.out.println("[DATA LOAD BINARY]");

        @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();

        setDomain(domain);
        objectInputStream.close();
        fileInputStream.close();
    }

    @NotNull @Override
    public String getName() {
        return NAME;
    }

    @NotNull @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
