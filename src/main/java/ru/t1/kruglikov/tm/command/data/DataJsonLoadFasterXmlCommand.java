package ru.t1.kruglikov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.t1.kruglikov.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull public static final String NAME = "data-load-json";
    @NotNull public static final String DESCRIPTION = "Load data in json file.";

    @SneakyThrows @Override
    public void execute() {
        System.out.println("[DATA LOAD JSON]");

        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_JSON));
        @NotNull final String json = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);

        setDomain(domain);
    }

    @NotNull @Override
    public String getName() {
        return NAME;
    }

    @NotNull @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
